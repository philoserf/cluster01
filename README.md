# Cluster01

> begin with the end in mind

## Goal

- [federated][fed] [highly-available][ha] [self-hosted][sh]
  [kubernetes][k8s] cluster that can be managed via [gitops][gitops]

## Principles

- start small/simple
- incremental build/improvement
- no pets, only cattle

## Tooling

- active kubernetes and cnct projects/tools
- coreos container linux
- terraform

## Administrivia

### Contributing

- fork, branch, test, push
- create a pull request
- respond to comments
- prosper

### Code of Conduct

- treat others as you would like to be treated

### Copyright & License

- Copyright 2018 by [Mark Ayers][ma]. Apache 2.0 [License][lic]

[fed]: https://kubernetes.io/docs/concepts/cluster-administration/federation/
[gitops]: https://www.weave.works/blog/gitops-modern-best-practices-for-high-velocity-application-development
[ha]: https://kubernetes.io/docs/admin/high-availability/building/
[k8s]: https://kubernetes.io
[lic]: LICENSE.md
[ma]: mailto:mark+cluster01@philoserf.com
[sh]: https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/#self-hosting
